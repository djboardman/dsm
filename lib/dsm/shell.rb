require_relative './command'
require 'pathname'
require 'readline'


module Dsm
  class Shell
LIST = ['system', 'component'].sort
    attr_reader :pwd
    def initialize pathname
      @pwd = pathname
      @systems = []
      @system = nil
    end

    def parse command_str
      Dsm::Command::Transform.new.apply(Dsm::Command::Parser.new.parse(command_str))
    end

    def execute command_str
      command = parse command_str
      self.send command.name, command
    rescue Parslet::ParseFailed
        Response.new :error, nil, "Syntax error"
    rescue NoMethodError
        Response.new :error, nil, "#{command_str}: command not found"
    end

    def set_pwd path_str
      pathname = path_str_to_pathname path_str
      if pathname.directory?
        @pwd = pathname
        return true
      else
        return false
      end
    end

    def path_str_to_pathname path_str
      pathname = Pathname.new(path_str)
      pathname = path_str.match?('^~') ? pathname.expand_path : pathname
      pathname = pathname.absolute? ? pathname : @pwd + pathname
    end

    def pwd_command *args
      Response.new :ok, @pwd.expand_path.to_s
    end

    def cd_command *args
      set_pwd(args[0].path_str) ? Response.new(:ok, nil) : Response.new(:error, nil, "No such directory")
    end

    def ls_command *args
      Response.new :ok, Dir.entries(@pwd)
    end

    def system_load_command *args
      pathname = path_str_to_pathname args[0].path_str
      if pathname.file?
        loaded_system = Dsm::Lang.load pathname
        @systems << loaded_system
        @system ||= loaded_system
        Response.new :ok, nil
      else
        Response.new :error, nil, "No such file"
      end
    rescue Errno::ENOENT
      Response.new :error, nil, "#{args[0].params[:pathname]}: file not found"
    rescue Parslet::ParseFailed => error
      msg = error.parse_failure_cause.children
      Response.new :error, nil, msg
    end

    def system_list_command *args
      Response.new :ok, @systems.map{ |s| s.name}
    end

    def system_set_command *args
      if (s = @systems.find{ |s| s.name == args[0].system_name}) == nil
        Response.new :error, nil, "System `#{args[0].system_name}` is not loaded"
      else
        @system = s
        Response.new :ok, s.name
      end
    end
    
    def system_current_command *args
      if @system != nil
        Response.new :ok, @system.name
      else
        response_no_current_system
      end
    end

    def system_output_command *args
      if @system != nil
        command = args[0]
        html = Dsm::Format::Output::Html.write(@system)
        filename = "#{@pwd}/#{@system.name}.html" 
        File.write("#{@pwd}/#{@system.name}.html", html)
        Response.new :ok, filename
      else
        response_no_current_system
      end
    end




    def component_list_command *args
      if @system != nil
        Response.new :ok, @system.components_sorted.map{ |c| "#{c.name}#{c.name == c.label ? '' : ' ' + c.label}" }
      else
        response_no_current_system
      end
    end

    def component_show_command *args
      command = args[0]
      if @system != nil
        if (c = @system.component(command.component_name)) != nil
          Response.new :ok, show_component(c)
        else 
          Response.new :error, nil, "#{command.component_name}: no component found"
        end
      else
        response_no_current_system
      end
    end

    def response_no_current_system
      Response.new :error, nil, "No system has been loaded"
    end

    def show_component c
      str_array = []
      str_array << "label: #{c.label}"
      str_array << " name: #{c.name}"
      str_array << "   ->: #{c.interactions_to_str}"
      str_array << "   <-: #{c.interactions_from_str}"
    end

    def current_system
      @system
    end

    def has_current_system?
      @system == nil ? false : true
    end

    def system scoped_name
      @systems.find{ |s| s[:scoped_name] == scoped_name }
    end

    class Response
      attr_reader :status, :std_out, :std_err
      def initialize status, std_out, std_err = nil
        @status = status
        @std_out = std_out
        @std_err = std_err
      end
    end

    class Interactive
      def initialize
        shell = Dsm::Shell.new(Pathname.new(Dir.pwd))
        stty_save = `stty -g`.chomp
        comp = proc { |s| (shell.pwd.glob("#{s}*").map{ |p| p.basename.to_s }).grep(/^#{Regexp.escape(s)}/) }
        Readline.completion_append_character = " "
        Readline.completion_proc = comp
        begin
          while buf = Readline.readline("DSM> ", true)
            response = shell.execute buf
            puts response.std_out if response.std_out != nil
            puts response.std_err if response.std_err != nil
          end
        rescue Interrupt => e
          system('stty', stty_save)
          puts ''
          exit
        end
      end
    end
  end
end