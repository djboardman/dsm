module Dsm
  class Interaction
    attr_reader :other_component, :attrs

    def initialize system, other_component, attrs = {}
      @other_component = other_component
      @attrs = attrs;
      @system = system
    end

    def set_system  system
      @system = system
    end

    def attr key
      @attrs[key] || @system.interaction_attrs[key]
    end

    

  end
end