
module Dsm
  class System
    attr_reader :name, :components, :interactions
    attr_reader :attrs, :component_attrs, :interaction_attrs
                
    def initialize name
      @name = name
      @components = []
      @interactions = []
      @component_attrs = {}
      @attrs = {}
      @interaction_attrs = {}
      @attrs = {}
    end

    def add_component name, attrs = {}
      if (c = component(name)) == nil
        c = Dsm::Component.new self, name, attrs
        @components << c
      else
        c.merge! attrs
      end
      return c
    end
    
    def add_symmetric_interaction component_1_name, component_2_name, attrs = {}
      add_asymmetric_interaction component_1_name, component_2_name, attrs
      add_asymmetric_interaction component_2_name, component_1_name, attrs
    end

    def add_asymmetric_interaction component_1_name, component_2_name, attrs = {}
      component_1 = add_component component_1_name
      component_2 = add_component component_2_name
      component_1.add_interaction_to Dsm::Interaction.new(self, component_2, attrs)
      component_2.add_interaction_from Dsm::Interaction.new(self, component_1, attrs)
    end

    def add_attr_for type, attrs
      case type
      when :component
        @component_attrs.merge! attrs
      when :system
        @attrs.merge! attrs
      when :interaction
        @interaction_attrs.merge! attrs
      end
    end

    def component_count
      @components.count
    end

    def component name
      @components.find { |c| c.name == name }
    end
 
    def components_sorted 
      @components.sort_by{ |c| c.name }
    end

    def components_sorted_by_index i
      components_sorted[i]
    end

    def component_index_by_name 
      Hash[@components.map.with_index { |c, n| [c.name, n] }]
    end

    def component_index_by_index
      Hash[@components.map.with_index { |c, i| [i, c.name] } ]
    end

    def table
      row0 = ["", "&#8599;"]
      row0 += components_sorted.map.with_index{ |c, i| (65 + i).chr }  
      rows = []
      rows += components_sorted.map.with_index{ |c, i| row c, i }
      [row0] + rows
    end

    def row c, i
      row = [c.label, (65 + i).chr]
      row += components_sorted.map.with_index{ |other_c, other_i| cell c, i, other_c, other_i }
    end

    def cell c, i, other_c, other_i
      if i == other_i
        (65 + i).chr
      else
        c.interaction_to(other_c.name) == nil ? "" : "X"
      end
    end


  end
end