module Dsm
  class Component
    attr_reader :name, :attrs
    def initialize system, name, attrs = {}
      @name = name
      @attrs = attrs
      @interactions_to = []
      @interactions_from = []
      @system = system
    end

    def label
      @attrs["label"] || name
    end

    def attr key
      @attrs[key] || @system.component_attrs[key]
    end

    def add_interaction_to interaction
      @interactions_to << interaction
    end

    def add_interaction_from interaction
      @interactions_from << interaction
    end

    def merge! attrs
      @attrs.merge! (attrs || {})
    end

    def interactions_to 
      @interactions_to.sort_by{ |i| i.other_component.name }.map{ |i| i.other_component }
    end

    def interactions_from
      @interactions_from.sort_by{ |i| i.other_component.name }.map{ |i| i.other_component }
    end

    def interactions_to_str
      interactions_to.map{ |c| "#{c.label} (#{c.name})" }.join(', ')
    end

    def interactions_from_str
      interactions_from.map{ |c| "#{c.label} (#{c.name})" }.join(', ')
    end

    def interaction_to other_component_name
      @interactions_to.find{ |i| i.other_component.name == other_component_name }
    end

  end
end