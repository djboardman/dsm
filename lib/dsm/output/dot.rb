module Dsm
  module Format
    module Output
      class Dot

        def initialize system
          @output = ""
          @output << self.open
          @output << components(system)
          @output << close
        end
        
        def write
          @output
        end

    private

        def components system
          output = ""
          id = 1
          system.components.each do |e|
            output << "\n #{e.id} [label=\"#{e.name}\"];" # [label=\"#{e.name}\"]"
            id += 1
          end

          system.interactions.each do |i|
            if i.is_symmetric
              output << "\n #{i.component_1.id} -> #{i.component_2.id} [dir=both, label=#{i.measure}];"
            end
          end
          return output
        end

        def open 
          "digraph G {"
        end

        def close
          "\n}"
        end
      end
    end
  end
end