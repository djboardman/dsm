module Dsm
  module Format
    module Output
      class Html

        def self.write system
          Dsm::Format::Output::Html.new(system).write
        end

        def initialize system
          @html = css
          @html << '<table class="dsm-table">'
          table = system.table

          table.each_with_index do |t, i|
            if i == 0
              @html << outer_tag("tr", "th", "dsm-table-h", t)
            else
              @html << outer_tag("tr", "td", "dsm-table-d", t)
            end
          end
          @html << "\n</table>"

        end

        def write 
          @html
        end

        def css
          css = '<style type="text/css">'
          css << '.dsm-table {border-collapse:collapse;border-spacing:0;}'
          css << '.dsm-table td{font-family:Arial, sans-serif;font-size:14px;padding:10px 15px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}'
          css << '.dsm-table th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 15px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}'
          css << '</style>'
        end

        def outer_tag outer_tag, inner_tag, inner_class, inner_texts
          w = ["\n", outer_tag, inner_tags(inner_tag, inner_class, inner_texts), outer_tag]
          "%s<%s>%s</%s>" % w
        end

        def inner_tags tag, css_class, texts
          s = ""
          texts.each do |t|
            s << inner_tag(tag, css_class, t)
          end
          return s
        end

        def inner_tag tag, css_class, text
          w = [tag, css_class, text, tag]
          "<%s class=\"%s\">%s</%s>" % w
        end

      end
    end
  end
end
