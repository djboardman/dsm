require 'parslet'


module Dsm
  module Lang 

    def self.load file_path
      f = File.open file_path
      code = f.read
      f.close
      compile code
    end

    def self.compile code
      tree = Parser.new.parse(code)
      Transform.new.apply(tree)
    end

    class Parser < Parslet::Parser
      rule(:space)            { match('\s').repeat(1) }
      rule(:space?)           { space.maybe } 
      rule(:colon)            { space? >> str(';') >> space? }
      rule(:kw_absent)        { kw_component.absent? >> kw_system.absent? >> kw_interaction.absent? }
      rule(:id)               { kw_absent >> match('[a-zA-Z_0-9]').repeat(1).as(:string) }
      rule(:value)            { space? >> str('"') >> match('[\sa-zA-Z_0-9]').repeat.as(:string) >> str('"') >> space?}

      rule(:symm_op)          { space? >> str('<->') >> space? }
      rule(:asymm_op)         { space? >> str('->') >> space? }
      rule(:equals)           { space? >> str('=') >> space? }
      rule(:comma)            { space? >> str(',') >> space? }
      rule(:bra)              { space? >> str('[') >> space? }
      rule(:ket)              { space? >> str(']') >> space? }
      rule(:kw_component)     { str('component') }
      rule(:kw_system)        { str('system') }
      rule(:kw_interaction)   { str('interaction') } 

      rule(:attr)             { (space? >> id.as(:key) >> equals >> value.as(:value) >> space?).as(:attr) >> space? }
      rule(:attr_list)        { (space? >> bra >> (attr >> (comma >> attr).repeat).as(:attrs) >> ket) >> space? }

      rule(:comp_attrs)       { (kw_component.as(:type) >> attr_list.maybe.as(:attr_list)).as(:attrs_for) }
      rule(:system_attrs)     { (kw_system.as(:type) >> attr_list.maybe.as(:attr_list)).as(:attrs_for) }
      rule(:inter_attrs)      { (kw_interaction.as(:type) >> attr_list.maybe.as(:attr_list)).as(:attrs_for) }
      rule(:system_attr_stmt) { (attr.as(:attrs)).as(:system_attr_stmt)}
      rule(:attrs_of_stmt)    { space? >> (system_attr_stmt | comp_attrs | system_attrs | inter_attrs) >> space? >> colon >> space? }
      rule(:attrs_of_stmts)   { space? >> str('attributes') >> space? >> str('{') >> attrs_of_stmt.repeat >> str('}') >> space?}
      
      rule(:comp_stmt)        { space? >> (id.as(:id) >> attr_list.maybe.as(:attr_list) >> space? >> colon).as(:comp_stmt) >> space? }
      rule(:comp_stmts)       { space? >> str('components') >> space? >> str('{') >> comp_stmt.repeat >> str('}') >> space? }

      rule(:symm_inter)       { (id.as(:lhs) >> symm_op >> id.as(:rhs) >> attr_list.maybe.as(:attr_list)).as(:symm_inter) } 
      rule(:asymm_inter)      { (id.as(:lhs) >> asymm_op >> id.as(:rhs) >> attr_list.maybe.as(:attr_list)).as(:asymm_inter)  } 
      rule(:inter_stmt)       { space? >> (symm_inter | asymm_inter) >> space? >> colon >> space? }
      rule(:inter_stmts)      { space? >> str('interactions') >> space? >> str('{') >> inter_stmt.repeat >> str('}') >> space? }    
      
      #rule(:stmt)             { (system_attr_stmt | attrs_of_stmt | inter_stmt | comp_stmt ) >> colon }
      #rule(:stmts)            {  space? >> stmt.repeat.as(:stmts) >> space? }
      rule(:stmts)            { attrs_of_stmts.maybe >> comp_stmts.maybe >> inter_stmts.maybe }
      rule(:system)           { (space? >> kw_system >> space? >> id.as(:id) >> space? >> str('{') >> stmts.as(:stmts) >> str('}') >> space?).as(:system) }

      root :system
    end

    CompStmt = Struct.new(:id, :attr_list) do
      def process system
        system.add_component id, attr_list ? attr_list.process : {}
      end
    end

    Attr = Struct.new(:k, :v) do
    end

    AttrList = Struct.new(:attrs) do
      def process
        attrs.map { |a| [a.k, a.v] }.to_h
      end
    end

    SymmInterStmt = Struct.new(:lhs, :rhs, :attr_list) do
      def process system
        system.add_symmetric_interaction lhs, rhs, attr_list ? attr_list.process : {}
      end
    end

    AsymmInterStmt = Struct.new(:lhs, :rhs, :attr_list) do
      def process system
        system.add_asymmetric_interaction lhs, rhs, attr_list ? attr_list.process : {}
      end
    end

    AttrsForStmt = Struct.new(:type, :attr_list) do
      def process system
        system.add_attr_for type, attr_list ? attr_list.process : {}
      end
    end

    System = Struct.new(:id, :stmts) do
      def process
        system = Dsm::System.new id
        stmts.each { |s| s.process system }
        return system
      end
    end
    
    class Transform < Parslet::Transform
      rule(string: simple(:x))            { String(x) }
      rule(attr: subtree(:x))             { Attr.new x[:key], x[:value] }
      rule(:attrs => subtree(:x))         { AttrList.new [x].flatten }
      rule(comp_stmt: subtree(:x))        { CompStmt.new x[:id], x[:attr_list] }
      rule(symm_inter: subtree(:x))       { SymmInterStmt.new x[:lhs], x[:rhs], x[:attr_list] }
      rule(attrs_for: subtree(:x))        { AttrsForStmt.new x[:type].to_sym, x[:attr_list] }
      rule(asymm_inter: subtree(:x))      { AsymmInterStmt.new x[:lhs], x[:rhs], x[:attr_list] }
      rule(system_attr_stmt: subtree(:x)) { AttrsForStmt.new :system, x }
      rule(:system => subtree(:x))        { System.new(x[:id], x[:stmts]).process }
    end

  end
end
