require 'pathname'

module Dsm
  module Command
    class Parser < Parslet::Parser
      rule(:space)     { match('\s').repeat(1) }
      rule(:space?)    { space.maybe } 
      rule(:path_str)  { space? >> match('[a-zA-Z_0-9/\.~]').repeat(1).as(:string) >> space? }
      rule(:name)      { space? >> match('[a-zA-Z_0-9]').repeat(1).as(:string) >> space? } 

      rule(:cd_keyword) { space? >> str('cd') >> space? }
      rule(:cd_command) { (cd_keyword >> path_str.as(:path_str)).as(:cd_command) }

      rule(:ls_keyword) { space? >> str('ls') >> space? }
      rule(:ls_command) { ls_keyword.as(:ls_command) }
      
      rule(:pwd_keyword)         { space? >> str('pwd') >> space? }
      rule(:pwd_command)         { pwd_keyword.as(:pwd_command) }

      rule(:file_commands)       { cd_command | ls_command | pwd_command }

      rule(:system_kw)           { str('system') | str('s')}
      rule(:system_load_keyword) { space? >> system_kw >> space >> str('load') >> space? }
      rule(:system_load_command) { (system_load_keyword >> path_str.as(:path_str)).as(:system_load_command) }

      rule(:system_list_keyword) { space? >> system_kw >> space >> str('list') >> space? }
      rule(:system_list_command) { system_list_keyword.as(:system_list_command)}

      rule(:system_set_keyword)  { space? >> system_kw >> space >> str('set') }
      rule(:system_set_command)  { (system_set_keyword >> space >> name.as(:name)).as(:system_set_command)}

      rule(:system_current_keyword)  { space? >> system_kw >> space >> str('current') }
      rule(:system_current_command)  { system_current_keyword.as(:system_current_command)}

      rule(:format_option)         { (str('html').as(:symbol) | str('dot').as(:symbol)).as(:format)}
      rule(:system_output_keyword) { space? >> system_kw >> space >> str('output') >> space >> format_option }
      rule(:system_output_command) { system_output_keyword.as(:system_output_command)}


      rule(:system_commands) { system_load_command | system_list_command | system_set_command | system_current_command | system_output_command }

      rule(:command_kw)             { str('component') | str('c')}
      rule(:component_list_keyword) { space? >> command_kw >> space >> str('list') >> space? }
      rule(:component_list_command) { component_list_keyword.as(:component_list_command)}

      rule(:component_show_keyword) { space? >> command_kw >> space >> str('show') >> space? }
      rule(:component_show_command) { (component_show_keyword >> name.as(:name)).as(:component_show_command)}

      rule(:component_commands) { component_list_command | component_show_command }

      rule(:command) { file_commands | system_commands | component_commands }
      rule(:root)    { command }
    end

    class Transform < Parslet::Transform
      rule(string: simple(:x))                 { String(x)}
      rule(symbol: simple(:x))                 { String(x).to_sym }
      #rule(pathname: simple(:x))               { Pathname.new(x) }
      rule(cd_command: subtree(:x))             { Command.new(:cd_command, {path_str: x[:path_str]}) }
      rule(ls_command: simple(:x))             { Command.new(:ls_command) }
      rule(pwd_command: simple(:x))            { Command.new(:pwd_command) }
      rule(system_load_command: subtree(:x))    { Command.new(:system_load_command, {path_str: x[:path_str]}) }
      rule(system_list_command: simple(:x))    { Command.new(:system_list_command) }
      rule(system_set_command: subtree(:x))    { Command.new(:system_set_command, {system_name: x[:name]}) }
      rule(system_current_command: simple(:x)) { Command.new(:system_current_command) }
      rule(system_output_command: subtree(:x)) { Command.new(:system_output_command, {format: x[:format]})}
      rule(component_list_command: simple(:x)) { Command.new(:component_list_command) }
      rule(component_show_command: subtree(:x)){ Command.new(:component_show_command, {component_name: x[:name]}) }
    end

    class Command
      attr_reader :name, :params
      def initialize name, params = {}
        @name = name
        @params = params
      end

      def method_missing method_name, *args, &block
        @params[method_name] != nil ? @params[method_name] : super
      end

      def respond_to_missing? method_name, *args
        if @params[method_name] != nil
          true
        else
          super
        end
      end
    end
    
  end
end