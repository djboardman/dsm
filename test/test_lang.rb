require 'minitest/autorun'
require 'dsm'
require 'common/common_input_tests'
include Dsm::Lang

class LangTest < Minitest::Test

  def setup
    @system = Dsm::Lang.load('test/common/common.dsm')
    #@system = Transform.new.apply(Parser.new.parse(code))
    @components = ['c1', 'c2', 'c3', 'c4'].map{|id| [id, @system.component(id)] }.to_h

  rescue Parslet::ParseFailed => failure
    puts failure.parse_failure_cause.ascii_tree
  end

  include CommonInputTests

end