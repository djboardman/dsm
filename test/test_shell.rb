require 'minitest/autorun'
require 'dsm'
require 'pathname'

class TestShell < Minitest::Test

  def setup
    @shell = Dsm::Shell.new Pathname.new(Dir.pwd)
  end

  def test_pwd
    assert_equal Dir.pwd,  @shell.execute('pwd').std_out
  end

  def test_ls
    cmd = 'ls'
    assert_equal ['.', '..'], @shell.execute(cmd).std_out[0..1]
  end

  def test_cd_relative
    cmd = 'cd test'
    @shell.execute(cmd)
    assert_equal Pathname(Dir.pwd) + Pathname('test'), @shell.pwd
  end

  def test_cd_to_home
    cmd = 'cd ~'
    @shell.execute(cmd)
    assert_equal File.expand_path('~'), @shell.pwd.to_s
  end

  def test_system_load
    cmd = 'system load test/common/common.dsm'
    out = @shell.execute(cmd)
    assert_nil out.std_out
    assert_equal :ok, out.status
  end

  def test_system_load_fail
    cmd = 'system load none_such' 
    out = @shell.execute(cmd)
    assert_equal :error, out.status
  end

  def test_system_list
    @shell.execute 'system load test/common/common.dsm'
    cmd = 'system list'
    assert_equal ['common'], @shell.execute(cmd).std_out
  end

  def test_system_set
    @shell.execute 'system load test/common/common.dsm'
    assert_equal 'common', @shell.execute('system set common').std_out
  end

  def test_system_set_fail
    assert_equal :error, @shell.execute('system set xyz').status
  end

  def test_system_current
    @shell.execute 'system load test/common/common.dsm'
    assert_equal 'common', @shell.execute('system current').std_out
  end

  def test_system_current_fail
    out = @shell.execute('system current')
    assert_nil out.std_out
    assert_equal :error, out.status
  end

  def test_system_output_html
    load_common
    home_path = File.expand_path '~'
    @shell.execute("cd #{home_path}")
    out = @shell.execute('system output html')
    assert_equal :ok, out.status
    assert_equal "#{home_path}/common.html", out.std_out
  end

  def test_system_output_html_fail
    out = @shell.execute('system output html')
    assert_equal :error, out.status
  end


  def test_component_list
    load_common
    out = @shell.execute('component list')
    assert_equal ['c1 Component One', 'c2', 'c3', 'c4', 'c5', 'c6'], out.std_out
  end

  def test_component_list_fail_not_loaded
    out = @shell.execute('component list')
    assert_equal :error, out.status
  end

  def test_component_show
    load_common
    out = @shell.execute('component show c1')
    assert_equal ["label: Component One", " name: c1", "   ->: c2 (c2)", "   <-: c2 (c2)"], out.std_out
  end

  def test_component_show_fail_no_system
    out = @shell.execute('component show c1')
    assert_equal :error, out.status
  end

  def test_component_show_fail_no_component
    load_common
    out = @shell.execute('component show x1')
    assert_equal :error, out.status
  end

  def load_common
    @shell.execute 'system load test/common/common.dsm'
  end



end