require 'minitest/autorun'
require 'dsm'

class TestCommand < Minitest::Test

  def test_cd
    command = parse 'cd ./test'
    assert_equal :cd_command, command.name
    assert_equal './test', command.path_str
  end

  def test_cd_home
    command = parse 'cd ~'
    assert_equal :cd_command, command.name
    assert_equal '~', command.path_str
  end

  def test_ls
    command = parse ('ls')
    assert_equal :ls_command, command.name
  end

  def test_pwd
    command = parse ('pwd')
    assert_equal :pwd_command, command.name
  end

  def test_system_load
    command = parse 'system load common/common.dsm'
    assert_equal :system_load_command, command.name 
    assert_equal 'common/common.dsm', command.path_str
  end

  def test_system_ls
    command = parse 'system list'
    assert_equal :system_list_command, command.name
  end

  def test_system_set
    command = parse 'system set common'
    assert_equal :system_set_command, command.name
    assert_equal 'common', command.system_name
  end

  def test_system_current
    command = parse 'system current'
    assert_equal :system_current_command, command.name
  end

  def test_system_output_html
    command = parse 'system output html'
    assert_equal :system_output_command, command.name
    assert_equal :html, command.format 
  end

  def test_component_list
    command = parse 'component list'
    assert_equal :component_list_command, command.name
  end

  def test_component_show
    command = parse 'component show c1'
    assert_equal :component_show_command, command.name
  end


  def parse cmd
    tree = Dsm::Command::Parser.new.parse(cmd)
    command = Dsm::Command::Transform.new.apply(tree)
    return command
  end

end