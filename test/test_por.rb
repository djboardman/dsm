require 'minitest/autorun'
require 'dsm'
require 'common/common_input_tests'





class TestPor < Minitest::Test
  def setup
    @system = Dsm::System.new 'common'

    @system.add_attr_for :component, {'c1' => 'a'}
    @system.add_attr_for :component, {'c2' => 'b', 'c3' => 'c'}
    @system.add_attr_for :system, {'s1' => 'a'}
    @system.add_attr_for :system, {'s2' => 'b', 's3' => 'c'}
    @system.add_attr_for :interaction, {'i1' => 'a'}
    @system.add_attr_for :interaction, {'i2' => 'b', 'i3' => 'c'}
    @system.add_attr_for :system, {'s4' => 'd'}
    @system.add_component 'c1', {'c11' => 'a', 'c12' => 'b'}
    @system.add_component 'c2', {'c21' => 'a', 'c22' => 'b'}
    @system.add_component 'c3', {'c31' => 'a'}
    @system.add_component 'c4', {'c41' => 'a', 'c42' => 'b'}
    @system.add_component 'c5'
    @system.add_asymmetric_interaction 'c3', 'c4', {'c3c41' => 'a'}
    @system.add_symmetric_interaction 'c1', 'c2', {'c1c21' => 'a'}
    @system.add_asymmetric_interaction 'c5', 'c6'

    @components = ['c1', 'c2', 'c3', 'c4'].map{|id| [id, @system.component(id)] }.to_h
  end

  def test_dump_dot
    #puts Dsm::Format::Output::Dot.new(@system).write
  end

  def test_dump_html
    #puts Dsm::Format::Output::Html.write @system
  end

  include CommonInputTests
end