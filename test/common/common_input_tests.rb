
require 'minitest/autorun'
require 'dsm'

module CommonInputTests

  def test_system
    assert_equal 'common', @system.name
  end

  def test_components
    assert_equal 'c1', @components['c1'].name
    assert_equal 'c2', @components['c2'].name
    assert_equal 'c3', @components['c3'].name
    assert_equal 'c4', @components['c4'].name
  end

  def test_component_attrs
    assert_equal 'a', @components['c1'].attr('c11')
    assert_equal 'b', @components['c1'].attr('c12')
    assert_equal 'a', @components['c1'].attr('c1')

    assert_equal 'a', @components['c2'].attr('c21')
    assert_equal 'b', @components['c2'].attr('c22')

    assert_equal 'a', @components['c3'].attr('c31')

    assert_equal 'a', @components['c4'].attr('c41')
    assert_equal 'b', @components['c4'].attr('c42')
  end

  def test_symm_interactions
    assert_equal 'c2', @components['c1'].interaction_to('c2').other_component.name
    assert_equal 'c1', @components['c2'].interaction_to('c1').other_component.name
  end

  def test_asymm_interactions
    assert_equal 'c4', @components['c3'].interaction_to('c4').other_component.name
    assert_nil @components['c4'].interaction_to('c3')
  end

  def test_interaction_attrs
    assert_equal 'a', @components['c1'].interaction_to('c2').attr('i1')
    assert_equal 'b', @components['c1'].interaction_to('c2').attr('i2')
    assert_equal 'c', @components['c1'].interaction_to('c2').attr('i3')
    assert_equal 'a', @components['c3'].interaction_to('c4').attr('c3c41')

  end

  def test_system_attrs
    assert_equal 'a', @system.attrs['s1']
    assert_equal 'b', @system.attrs['s2']
    assert_equal 'c', @system.attrs['s3']
    assert_equal 'd', @system.attrs['s4']
  end
end