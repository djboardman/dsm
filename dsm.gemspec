Gem::Specification.new do |s|
  s.name         = 'dsm'
  s.version      = '0.0.0'
  s.date         = '2020-03-28'
  s.summary      = 'DSM matrix core'
  s.description  = 'A DSM matrix calculator'
  s.authors      = ['David Boardman']
  s.email        = 'djboardman@gmail.com'
  s.files        = Dir["{lib}/**/*"] + %w(Gemfile Rakefile)
  s.homepage     = 'https://asciiarch.org'
  s.licenses     = ['MIT']
  s.executables  << 'dsm'

  s.add_runtime_dependency 'parslet', '~> 1.8'
  s.add_runtime_dependency 'thor', '~> 1.0'
end